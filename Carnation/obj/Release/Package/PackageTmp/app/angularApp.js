﻿
var messApp = angular.module('messageApp', ['SignalR']);

messApp.service(
    "userServices",
    function ($http, $q) {

        return ({
            getUsers: getUsers,
        });
        // I get all of the users in the remote collection.
        function getUsers() {

            var request = $http({
                method: "get",
                url: "/Users/GetUserList",
                params: {
                    action: "get"
                }
            });

            return (request.then(handleSuccess, handleError));

        }

        // ---
        // PRIVATE METHODS.
        // ---

        function handleError(response) {


            if (
                !angular.isObject(response.data) ||
                !response.data.message
                ) {

                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }


        function handleSuccess(response) {

            return (response.data);

        }
    }
);

messApp.service(
    "tweetServices",
    function ($http, $q) {
        return ({
            loadTweets: loadTweets,
            viewingTweet: viewingTweet
        });

        function loadTweets() {

            var request = $http({
                method: "get",
                url: "/Message/GetUserMessages",
                params: {
                    action: "get"
                }
            });

            return (request.then(handleSuccess, handleError));

        };

        function viewingTweet(tweet) {
            var request = $http({
                url: '/Message/UpdateTweet',
                method: "POST",
                data: { 'tweetId': tweet.Id, 'tweetStatus': tweet.Seen }
            }).then(function (response) {
                // success
            });
        }

        function handleError(response) {
            if (
                !angular.isObject(response.data) ||
                !response.data.message
                ) {

                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));

        }
        function handleSuccess(response) {

            return (response.data);

        }

    }
);

messApp.factory('hubServices', ['$rootScope', '$http', 'tweetServices', 'Hub', function ($rootScope, $http, tweetServices, Hub) {
    var Tweets = this;
    Tweets.messages = [];

    Tweets.result = $http({
        method: "get",
        url: "/Message/GetUserMessages"
    });

    //Hub setup SignalR 
    var hub = new Hub('tweets', {
        'getMessage': function (tweet) {
            $rootScope.tweets.push(tweet);
            $rootScope.$apply();
        }
    }, ['lock']);

    return Tweets;
}])

messApp.controller('MessageController', ['$rootScope', '$scope', 'hubServices', 'userServices', 'tweetServices', function ($rootScope, $scope, hubServices, userServices, tweetServices) {
    $rootScope.tweets = [];
    hubServices.result.then(function (response) {
        angular.forEach(response.data, function (item) {
            $rootScope.tweets.push(item);
        });
    });

    $scope.userList = [];

    loadRemoteData();

    updateTweets = function (text) {
        $scope.tweets.messages.push(text);
    }

    $scope.watchTweet = function (tweet) {
        tweetServices.viewingTweet(tweet);
    };
    function applyUserRemoteData(newUsers) {
        $scope.userList = newUsers;
    }

    $scope.customFilter = function (data) {
        if ($scope.selectedUser === null || $scope.selectedUser === undefined)
            return true;
        if (data.FromUserName === $scope.selectedUser.UserName || data.ToUserName === $scope.selectedUser.UserName) {
            return true;
        }
        else {
            return false;
        }
    };

    function loadRemoteData() {
        userServices.getUsers().then(
                function (users) {
                    applyUserRemoteData(users);
                }
            );

    }
}]);

