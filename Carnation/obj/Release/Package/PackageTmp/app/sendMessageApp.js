﻿var app = angular.module('app', ['ui.bootstrap']);

function messageController($scope, $http, $log) {

    $scope.hasError = false;
    $scope.isSucces = false;

    $scope.getUsers = function (user) {
        return $http.get('/Users/Search', {
            params: {
                selectedUser: user
            }
        }).then(function (res) {
            var users = [];
            angular.forEach(res.data, function (item) {
                users.push(item);
            });
            return users;
        });
    };

    $scope.sendMessage = function () {
        var message = $scope.messageForSend;
        var userId = $scope.selectedUser.UserId;

        $scope.hasError = false;
        $scope.isSucces = false;

        var request = $http({
            method: "post",
            url: "/Message/SendMessage",
            data: {
                userId: userId,
                message: message
            }
        })
        .success(function (response) {
            if (response.success == false) {
                $scope.hasError = true;
                $scope.errorMessage = response.resultText;
            }
            else {
                $scope.isSucces = true;

            }

        });

    };

}

