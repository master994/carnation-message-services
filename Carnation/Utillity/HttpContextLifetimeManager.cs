﻿using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carnation.Utillity
{
    /// <summary>
    /// Saját LifeTimeManager a UnitOfWork apróságai miatt.
    /// </summary>
    public class HttpContextLifetimeManager : LifetimeManager
    {
        private object m_Key = new Object();

        private IDictionary Store
        {
            get
            {
                return HttpContext.Current.Items;
            }
        }

        public override object GetValue()
        {
            if (Store.Contains(m_Key))
            {
                return Store[m_Key];
            }
            else
            {
                return null;
            }
        }

        public override void RemoveValue()
        {
            if (Store.Contains(m_Key))
            {
                IDisposable disposable = Store[m_Key] as IDisposable;
                if (disposable != null)
                {
                    disposable.Dispose();
                }
                Store.Remove(m_Key);
            }
        }

        public override void SetValue(object newValue)
        {
            if (Store.Contains(m_Key))
            {
                if (Object.ReferenceEquals(Store[m_Key], newValue))
                {
                    return;
                }
                else
                {
                    RemoveValue();
                }
            }
            Store.Add(m_Key, newValue);
        }
    }
}