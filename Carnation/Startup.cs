﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using System.Web.Routing;

[assembly: OwinStartupAttribute(typeof(Carnation.Startup))]
namespace Carnation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //Owin config behúzása.
            ConfigureAuth(app);

            //SignalR hubot mappelése.
            app.MapSignalR();
        }
    }
}
