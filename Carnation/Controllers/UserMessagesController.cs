﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CarnationTest.Models;
using DataAccesLayer.Models;
using Microsoft.Practices.Unity;
using BusinessLayer.Interfaces;

namespace Carnation.Controllers
{
    [Authorize]
    public class UserMessagesController : ApiController
    {
        [Dependency]
        protected IMessageServices MessageService { get; set; }
        private CarnationContext db = new CarnationContext();

        // GET: api/UserMessages
        [Authorize]
        public List<UserMessage> GetMessages()
        {
            using (CarnationContext context = new CarnationContext())
            {
                context.Configuration.ProxyCreationEnabled = false;
                return context.Messages.ToList();
            }
        }

        // GET: api/UserMessages/5
        [ResponseType(typeof(UserMessage))]
        [Authorize]
        public IHttpActionResult GetUserMessage(int id)
        {
            UserMessage userMessage = db.Messages.Find(id);
            if (userMessage == null)
            {
                return NotFound();
            }

            return Ok(userMessage);
        }

        // PUT: api/UserMessages/5
        [ResponseType(typeof(void))]
        [Authorize]
        public IHttpActionResult PutUserMessage(int id, UserMessage userMessage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userMessage.Id)
            {
                return BadRequest();
            }

            db.Entry(userMessage).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserMessageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/UserMessages
        [ResponseType(typeof(UserMessage))]
        [Authorize]
        public IHttpActionResult PostUserMessage(UserMessage userMessage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = MessageService.SendMessage(userMessage.UserFromID, userMessage.UserToID, userMessage.Message);

            if (result.Response == BusinessLayer.MessageResponse.LimitReached)
            {
                ModelState.AddModelError("limit", "Limit reached");
                return BadRequest(ModelState);
            }
            else if (result.Response == BusinessLayer.MessageResponse.Banned)
            {
                ModelState.AddModelError("banned", "You're banned for this user.");
                return BadRequest(ModelState);
            }
            return RedirectToRoute("GetUserMessage", new { id = result.Message.Id });
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserMessageExists(int id)
        {
            return db.Messages.Count(e => e.Id == id) > 0;
        }
    }
}