﻿using BusinessLayer.Interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;


namespace Carnation.Controllers
{
    public class UsersController : Controller
    {
        [Dependency]
        protected IUserServices UserServices { get; set; }
        // GET: Users
        [Authorize]
        public ActionResult Index()
        {
            var currentUser = HttpContext.User.Identity.GetUserId();
            var model = UserServices.GetUserList(currentUser);
            return View(model);
        }

        public ActionResult ConfirmAccount()
        {
            return View("ConfirmAccount");
        }
        [Authorize]
        public ActionResult GetUserList()
        {
            var currentUser = HttpContext.User.Identity.GetUserId();

            var model = UserServices.GetUserList(currentUser);

            return Json(model.Users, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Search(string selectedUser)
        {
            var currentUser = HttpContext.User.Identity.GetUserId();

            var model = UserServices.SearchForUser(currentUser, selectedUser);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult SearchBan(string selectedUser)
        {
            var currentUser = HttpContext.User.Identity.GetUserId();

            var model = UserServices.SearchForBanUser(currentUser, selectedUser);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult BanList()
        {
            return View();
        }

        [Authorize]
        public ActionResult AddBan(string userId)
        {
            var currentUser = HttpContext.User.Identity.GetUserId();

            UserServices.AddBann(currentUser, userId);

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult RemoveBan(string userId)
        {
            var currentUser = HttpContext.User.Identity.GetUserId();

            UserServices.RemoveBann(currentUser, userId);

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetBanList(string userId)
        {
            var currentUser = HttpContext.User.Identity.GetUserId();

            var model = UserServices.GetBanList(currentUser);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetVisibility()
        {
            var currentUser = HttpContext.User.Identity.GetUserId();

            var model = UserServices.GetVisibility(currentUser);

            return Json(new { visibility = model }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult SetVisibility(bool visibility)
        {
            var currentUser = HttpContext.User.Identity.GetUserId();

            UserServices.SetVisibility(currentUser, visibility);

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
    }
}