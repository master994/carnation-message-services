﻿using BusinessLayer.Interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;
using BusinessLayer.Enums;
using BusinessLayer;

namespace Carnation.Controllers
{
    [System.Web.Mvc.Authorize]
    public class MessageController : Controller
    {
        [Dependency]
        protected IMessageServices MessageServices { get; set; }

        [System.Web.Mvc.Authorize]
        // GET: Message
        public ActionResult Index()
        {
            return View();
        }

        [System.Web.Mvc.Authorize]
        public ActionResult MessageSend()
        {
            return View();
        }

        [System.Web.Mvc.Authorize]
        [HttpPost]
        public ActionResult SendMessage(string userId, string message)
        {
            var currentUserId = HttpContext.User.Identity.GetUserId();

            var result = MessageServices.SendMessage(userId, currentUserId, message);

            if (result.Response == MessageResponse.LimitReached)
            {
                return Json(new { success = false, resultText = "Limit reached today.Try tomorrow." });
            }
            if (result.Response == MessageResponse.Banned)
            {
                return Json(new { success = false, resultText = "You can't send message for this user. You're banned." });
            }

            if (result.Response == MessageResponse.Success)
            {
                //Ha sikeres az üzenet akkor SignalR segítségével ha éppen az üzeneteit olvassa a vevő fél tovább terjesztjük
                //SignalR context
                var context = GlobalHost.ConnectionManager.GetHubContext<TweetsHub>();
                //Felhasználó minden connectionId-ra továbbküldjük az üzenetet
                context.Clients.Group(userId).getMessage(result.Message);

                return Json(new { success = true });
            }
            return Json(new { });


        }

        [System.Web.Mvc.Authorize]
        public ActionResult GetTweets()
        {

            return View();
        }

        [System.Web.Mvc.Authorize]
        public ActionResult GetUserMessages()
        {
            var currentUserId = HttpContext.User.Identity.GetUserId();

            var model = MessageServices.GetUserMessages(currentUserId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Mvc.Authorize]
        public ActionResult UpdateTweet(int tweetId, bool tweetStatus)
        {
            var status = MessageServices.UpdateTweet(tweetId, tweetStatus);

            if (status == TweetResponse.Success)
                return Json(new { success = true });

            return Json(new { success = false, errorMessage = "Can't update" });

        }
    }
}