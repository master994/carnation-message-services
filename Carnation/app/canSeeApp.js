﻿var canSeeApp = angular.module('canSeeApp', []);

canSeeApp.controller('editCtrl', ['$scope', '$http', '$log', function ($scope, $http, $log) {

    var promise = $http({ method: 'GET', url: '/Users/GetVisibility' }).
    success(function (data, status, headers, config) {
        $log.info(data.visibility);
        $scope.visibility = data.visibility;
    });

    $scope.setVisibility = function () {
        console.log('hey posting');
        var request = $http({
            method: "post",
            url: "/Users/SetVisibility",
            data: {
                visibility: $scope.visibility
            }
        });
    }
}]);