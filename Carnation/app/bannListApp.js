﻿var bannApp = angular.module('bannApp', ['ui.bootstrap']);

bannApp.controller('BanCtrl', ['$scope', '$http', '$log', function ($scope, $http, $log) {

    $scope.bannList = [];
    $scope.hasError = false;
    $scope.isSucces = false;

    $http.get('/Users/GetBanList/')
    .then(function (res) {
        $log.info(res);
        angular.forEach(res.data, function (item) {
            $scope.bannList.push(item);
        });
    });

    $scope.getUsers = function (user) {
        return $http.get('/Users/SearchBan', {
            params: {
                selectedUser: user
            }
        }).then(function (res) {
            var users = [];
            angular.forEach(res.data, function (item) {
                users.push(item);
            });
            return users;
        });
    };



    $scope.removeBann = function (userId) {

        var request = $http({
            url: '/Users/RemoveBan',
            method: "POST",
            data: { 'userId': userId }
        }).then(function (response) {
            var index = -1;
            var comArr = eval($scope.bannList);
            for (var i = 0; i < comArr.length; i++) {
                if (comArr[i].UserId === userId) {
                    index = i;
                    break;
                }
            }
            if (index === -1) {
                alert("Something gone wrong");
            }
            $scope.bannList.splice(index, 1);
        });

    }
    $scope.addBann = function () {
        var request = $http({
            url: '/Users/AddBan',
            method: "POST",
            data: { 'userId': $scope.selectedUser.UserId }
        }).then(function (response) {
            $scope.bannList.push($scope.selectedUser);
        });
    };
}]);
