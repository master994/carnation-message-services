﻿
var messApp = angular.module('messageApp', ['SignalR']);

angular.module('messageApp').service(
    "userServices",
    function ($http, $q) {
        return ({
            getUsers: getUsers,
        });

        function getUsers() {

            var request = $http({
                method: "get",
                url: "/Users/GetUserList",
                params: {
                    action: "get"
                }
            });

            return request;
        }

    }
);

angular.module('messageApp').service(
    "tweetServices",
    function ($http, $q) {
        return ({
            loadTweets: loadTweets,
            viewingTweet: viewingTweet
        });

        function loadTweets() {

            var request = $http({
                method: "get",
                url: "/Message/GetUserMessages",
                params: {
                    action: "get"
                }
            });

            return request;

        };

        function viewingTweet(tweet) {
            var request = $http({
                url: '/Message/UpdateTweet',
                method: "POST",
                data: { 'tweetId': tweet.Id, 'tweetStatus': tweet.Seen }
            });

            return request;

        }
    }
);

angular.module('messageApp')
.factory('hubServices',
['$rootScope', '$http', 'tweetServices', 'Hub', function ($rootScope, $http, tweetServices, Hub) {
    var Tweets = this;
    Tweets.messages = [];

    Tweets.init = function () {
        var request = $http({
            method: "get",
            url: "/Message/GetUserMessages"
        });

        request.then(function (data) {
            Tweets.messages = data;
        });
    }

    var hub = new Hub('tweets', {
        'getMessage': function (tweet) {
            Tweets.messages.push(tweet);
        }
    }, ['lock']);

    return Tweets;
}])

messApp.controller('MessageController', ['$rootScope', '$scope', 'hubServices', 'userServices', 'tweetServices', function ($rootScope, $scope, hubServices, userServices, tweetServices) {
    $scope.tweets = [];

    hubServices.init();

    $scope.tweets = hubServices.messages;

    $scope.userList = [];

    loadRemoteData();

    updateTweets = function (text) {
        $scope.tweets.messages.push(text);
    }

    $scope.watchTweet = function (tweet) {
        tweetServices.viewingTweet(tweet);
    };
    function applyUserRemoteData(newUsers) {
        $scope.userList = newUsers;
    }

    $scope.customFilter = function (data) {
        if ($scope.selectedUser === null || $scope.selectedUser === undefined)
            return true;
        if (data.FromUserName === $scope.selectedUser.UserName || data.ToUserName === $scope.selectedUser.UserName) {
            return true;
        }
        else {
            return false;
        }
    };

    function loadRemoteData() {
        userServices.getUsers().then(
                function (users) {
                    applyUserRemoteData(users);
                }
            );

    }
}]);

