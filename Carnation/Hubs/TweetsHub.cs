﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using BusinessLayer.Services;
using Microsoft.Practices.Unity;
using System.Collections.Concurrent;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR.Hubs;


namespace Carnation.Controllers
{
    [HubName("tweets")]
    public class TweetsHub : Hub
    {
        [Dependency]
        protected MessageServices MessageService { get; set; }

        public override Task OnConnected()
        {
            string userName = Context.User.Identity.GetUserId();
            Groups.Add(Context.ConnectionId, userName);

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled = true)
        {
            return base.OnDisconnected(stopCalled);
        }

    }
}