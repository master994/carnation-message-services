using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc3;
using BusinessLayer.Interfaces;
using BusinessLayer.Services;
using BusinessLayer.UnitOfWork;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CarnationTest.Models;
using System.Data.Entity;
using DataAccesLayer.Models;
using Microsoft.Owin.Security;
using Carnation.Controllers;
using Carnation.Utillity;
using Microsoft.Practices.Unity.InterceptionExtension;

namespace Carnation
{
    /// <summary>
    /// Unity container konfigur�ci�ja
    /// </summary>
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            //Felh�zzuk a Unity configot
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();
            container.RegisterType<IUnitOfWork, UnitOfWork>(new HttpContextLifetimeManager(), new InterceptionBehavior<PolicyInjectionBehavior>(), new Interceptor<TransparentProxyInterceptor>());

            container.RegisterType<IMessageServices, MessageServices>();
            container.RegisterType<IUserServices, UserServices>();

            container.RegisterType(typeof(UserManager<>),
           new InjectionConstructor(typeof(IUserStore<>)));
            container.RegisterType<Microsoft.AspNet.Identity.IUser>(new InjectionFactory(c => c.Resolve<Microsoft.AspNet.Identity.IUser>()));
            container.RegisterType(typeof(IUserStore<>), typeof(UserStore<>));
            container.RegisterType<IdentityUser, ApplicationUser>(new ContainerControlledLifetimeManager());
            container.RegisterType<UserManager<ApplicationUser>>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>(new HierarchicalLifetimeManager());

            container.RegisterType<AccountController>(new InjectionConstructor());
            return container;
        }
    }
}