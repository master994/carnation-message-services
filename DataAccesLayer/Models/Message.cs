﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccesLayer.Models
{
    public class UserMessage
    {
        public int Id { get; set; }
        public string UserFromID { get; set; }
        public virtual ApplicationUser UserFrom { get; set; }
        public string UserToID { get; set; }

        public virtual ApplicationUser UserTo { get; set; }
        public string Message { get; set; }

        public DateTime SentDate { get; set; }
        public bool Seen { get; set; }
    }
}

