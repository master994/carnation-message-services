﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccesLayer.Models
{
    public class Ban
    {
        public int Id { get; set; }
        public string WhoID { get; set; }
        public virtual ApplicationUser Who { get; set; }
        public string WhooseID { get; set; }

        public virtual ApplicationUser Whoose { get; set; }
    }
}
