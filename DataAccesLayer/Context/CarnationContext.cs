﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using DataAccesLayer.Models;

namespace CarnationTest.Models
{
    public class CarnationContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<UserMessage> Messages { get; set; }
        public DbSet<Ban> BlackList { get; set; }
        public CarnationContext()
            : base("CarnationTestConnection", throwIfV1Schema: false)
        {
        }

        public static CarnationContext Create()
        {
            return new CarnationContext();
        }
    }
}