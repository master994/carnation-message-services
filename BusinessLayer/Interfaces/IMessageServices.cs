﻿using BusinessLayer.Enums;
using BusinessLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Interfaces
{
    public interface IMessageServices
    {
        MessageResponseViewModel SendMessage(string userId, string currentUserId, string message);
        List<UserTweetViewModel> GetUserMessages(string userId);

        TweetResponse UpdateTweet(int tweetId, bool tweetStatus);
    }
}
