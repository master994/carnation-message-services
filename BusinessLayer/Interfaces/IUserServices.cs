﻿using BusinessLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Interfaces
{
    public interface IUserServices
    {
        UserListViewModel GetUserList(string currentUserId);
        List<UserViewModel> SearchForUser(string currentUserId, string selectedUser);
        List<UserViewModel> SearchForBanUser(string currentUserId, string selectedUser);
        void RemoveBann(string userId, string whoose);
        void AddBann(string userId, string whoose);

        List<UserViewModel> GetBanList(string userId);

        bool GetVisibility(string currentUserId);

        void SetVisibility(string currentUserId, bool visibility);

    }
}
