﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using CarnationTest.Models;
namespace BusinessLayer.Interfaces
{

    public interface IUnitOfWork : IDisposable
    {
        void SaveChanges();

        IDbSet<T> GetDbSet<T>() where T : class;


        IEnumerable<T> GetChangedEntities<T>() where T : class;

        CarnationContext Context { get; }

        void MarkModified<T>(T entity) where T : class;

        ITransaction BeginTransaction();

        DbEntityEntry<T> GetEntry<T>(T entity) where T : class;

        void SetPropertiesModified<T, TResult>(T entity, params Expression<Func<T, TResult>>[] selectors) where T : class;

        void SetPropertiesModified<T, TResult>(T entity, IEnumerable<Expression<Func<T, TResult>>> selectors) where T : class;
    }
}


