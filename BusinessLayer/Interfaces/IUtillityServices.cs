﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Interfaces
{
    public interface IUtillityServices
    {
        bool SendConfirmationEmail(string userName, string userId, string emailAddress, string token);
    }
}
