﻿using BusinessLayer.Interfaces;
using CarnationTest.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.UnitOfWork
{

    public class UnitOfWork : IUnitOfWork
    {
        protected DbContext context;
        public UnitOfWork()
        {
            context = new CarnationContext();
        }

        public void Dispose()
        {
            if (context != null)
            {
                context.Dispose();
                context = null;
            }
        }

        public void SaveChanges()
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public CarnationContext Context
        {
            get { return (CarnationContext)context; }
            protected set { context = value; }
        }


        public IDbSet<T> GetDbSet<T>()
            where T : class
        {
            return context.Set<T>();
        }

        public IEnumerable<T> GetChangedEntities<T>()
          where T : class
        {
            return Context.ChangeTracker.Entries<T>().Where(x => x.State == (EntityState.Added | EntityState.Deleted | EntityState.Modified)).Select(x => x.Entity);
        }

        public void MarkModified<T>(T entity) where T : class
        {
            var dbset = context.Set<T>();
            dbset.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }

        public void MarkModified<T>(params T[] entities) where T : class
        {
            MarkModified(entities as IEnumerable<T>);
        }

        public void MarkModified<T>(IEnumerable<T> entities) where T : class
        {
            foreach (var item in entities)
            {
                MarkModified(item);
            }
        }

        public ITransaction BeginTransaction()
        {
            return new TransactionWrapper(Context.Database.BeginTransaction());
        }

        public DbEntityEntry<T> GetEntry<T>(T entity) where T : class
        {
            return Context.Entry<T>(entity);
        }

        public void SetPropertiesModified<T, TResult>(T entity, IEnumerable<Expression<Func<T, TResult>>> selectors) where T : class
        {
            var entry = Context.Entry(entity);
            foreach (var item in selectors)
            {
                entry.Property(item).IsModified = true;
            }
        }

        public void SetPropertiesModified<T, TResult>(T entity, params Expression<Func<T, TResult>>[] selectors) where T : class
        {
            SetPropertiesModified<T, TResult>(entity, selectors as IEnumerable<Expression<Func<T, TResult>>>);
        }


        private sealed class TransactionWrapper : ITransaction
        {
            DbContextTransaction _txn = null;

            public TransactionWrapper(DbContextTransaction txn)
            {
                _txn = txn;
            }

            public void Commit()
            {
                _txn.Commit();
            }


            public void Rollback()
            {
                try
                {
                    _txn.Rollback();
                }
                catch (EntityException eexc)
                {

                    throw;
                }
            }

            public void Dispose()
            {
                _txn.Dispose();
            }
        }
    }
}
