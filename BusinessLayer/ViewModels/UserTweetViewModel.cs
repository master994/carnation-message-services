﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.ViewModels
{
    public class UserTweetViewModel
    {
        public int Id { get; set; }

        public string FromUserId { get; set; }
        public string ToUserId { get; set; }

        public string FromUserName { get; set; }
        public string ToUserName { get; set; }
        public string ToUserFullName { get; set; }
        public string Message { get; set; }

        public string FromUserFullName { get; set; }
        public bool Seen { get; set; }
        public bool IsDisabled { get; set; }
    }
}
