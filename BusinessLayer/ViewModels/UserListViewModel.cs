﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.ViewModels
{
    public class UserListViewModel
    {
        public List<UserViewModel> Users { get; set; }
    }
}
