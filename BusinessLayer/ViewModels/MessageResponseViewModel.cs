﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.ViewModels
{
    public class MessageResponseViewModel
    {
        public MessageResponse Response { get; set; }
        public UserTweetViewModel Message { get; set; }
    }
}
