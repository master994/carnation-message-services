﻿using BusinessLayer.Enums;
using BusinessLayer.Interfaces;
using BusinessLayer.ViewModels;
using DataAccesLayer.Models;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace BusinessLayer.Services
{
    /// <summary>
    /// Üzenet küldési service
    /// </summary>
    public class MessageServices : IMessageServices
    {
        [Dependency]
        protected IUnitOfWork UnitOfWork { get; set; }

        /// <summary>
        /// Üzenet küldés egy másik felhasználónak ( hiba esetén visszajelzés küldése)
        /// </summary>
        /// <param name="userId">Fogadó felhasználó azonosítója</param>
        /// <param name="currentUserId">Bejelentkezett felhasználó azonosítója</param>
        /// <param name="message">Üzenet szövege</param>
        /// <returns>Visszatér az üzenet státuszával és az entitás viewmodeljével</returns>
        public MessageResponseViewModel SendMessage(string userId, string currentUserId, string message)
        {
            var now = DateTime.Now;

            var messageCount = UnitOfWork.GetDbSet<UserMessage>()
                .Where(x => x.UserFromID == currentUserId
                    && x.SentDate.Year == now.Year
                    && x.SentDate.Month == now.Month
                    && x.SentDate.Day == now.Day).Count();

            if (messageCount >= 5)
                return new MessageResponseViewModel { Response = MessageResponse.LimitReached };

            if (UnitOfWork.GetDbSet<Ban>().Any(x => x.WhoID == userId && x.WhooseID == currentUserId))
                return new MessageResponseViewModel { Response = MessageResponse.Banned };

            using (var t = UnitOfWork.BeginTransaction())
            {
                try
                {
                    var insertedMessage = new UserMessage
                    {
                        UserFromID = currentUserId,
                        UserToID = userId,
                        Message = message,
                        SentDate = DateTime.Now
                    };

                    UnitOfWork.GetDbSet<UserMessage>().Add(insertedMessage);

                    UnitOfWork.SaveChanges();
                    t.Commit();

                    var newEntity = UnitOfWork.GetDbSet<UserMessage>()
                        .Include(x => x.UserFrom).Include(x => x.UserTo)
                        .FirstOrDefault(x => x.Id == insertedMessage.Id);


                    return new MessageResponseViewModel
                    {
                        Response = MessageResponse.Success,
                        Message = new UserTweetViewModel
                        {
                            FromUserFullName = newEntity.UserFrom.FullName,
                            FromUserId = newEntity.UserFrom.Id,
                            FromUserName = newEntity.UserFrom.UserName,
                            Id = newEntity.Id,
                            Message = newEntity.Message,
                            Seen = newEntity.Seen,
                            ToUserFullName = newEntity.UserTo.FullName,
                            ToUserId = newEntity.UserTo.Id,
                            ToUserName = newEntity.UserTo.UserName
                        }
                    };

                }
                catch (Exception ex)
                {
                    t.Rollback();
                    throw;
                }
            }
        }

        /// <summary>
        /// Visszaadja a bejelentkezett user összes üzenetét
        /// </summary>
        /// <param name="userId">Bejelentkezett felhasználó azonosító</param>
        /// <returns>Üzenet lista</returns>
        public List<UserTweetViewModel> GetUserMessages(string userId)
        {

            var model = UnitOfWork.GetDbSet<UserMessage>().Where(x => x.UserToID == userId || x.UserFromID == userId).Select(ut => new UserTweetViewModel
                        {
                            Id = ut.Id,
                            FromUserId = ut.UserFromID,
                            FromUserName = ut.UserFrom.UserName,
                            FromUserFullName = ut.UserFrom.FullName,
                            ToUserName = ut.UserTo.UserName,
                            ToUserFullName = ut.UserTo.FullName,
                            Message = ut.Message,
                            Seen = ut.Seen,
                            IsDisabled = ut.UserToID != userId
                        }).ToList();

            return model;
        }

        /// <summary>
        /// Frissíti az üzenet megtekintését
        /// </summary>
        /// <param name="tweetId"></param>
        /// <param name="tweetStatus"></param>
        /// <returns></returns>
        public TweetResponse UpdateTweet(int tweetId, bool tweetStatus)
        {
            using (var t = UnitOfWork.BeginTransaction())
            {
                try
                {
                    var tweet = UnitOfWork.GetDbSet<UserMessage>().FirstOrDefault(x => x.Id == tweetId);

                    if (tweet == null)
                        throw new Exception();

                    tweet.Seen = tweetStatus;

                    UnitOfWork.SaveChanges();
                    t.Commit();

                    return TweetResponse.Success;
                }
                catch (Exception)
                {
                    t.Rollback();
                    throw;
                }

            }
        }
    }
}
