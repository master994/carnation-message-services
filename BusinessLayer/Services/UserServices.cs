﻿using BusinessLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Interfaces;
using DataAccesLayer.Models;
using Microsoft.Practices.Unity;

namespace BusinessLayer.Services
{
    public class UserServices : IUserServices
    {
        [Dependency]
        protected IUnitOfWork UnitOfWork { get; set; }

        /// <summary>
        /// Vissza adja a regisztrált megerősített felhasználó listáját ( a bejelentkezett felhasználón kívül)
        /// </summary>
        /// <param name="currentUserId">Bejelenetkezett felhasználó azonosító</param>
        /// <returns>UserListViewModel</returns>
        public UserListViewModel GetUserList(string currentUserId)
        {
            var model = new UserListViewModel
            {
                Users = UnitOfWork.GetDbSet<ApplicationUser>().Where(x => x.CanList && x.EmailConfirmed && x.Id != currentUserId)
                .Select(n => new UserViewModel
                {
                    UserId = n.Id,
                    UserName = n.UserName,
                    UserFullName = n.FullName,
                    RegistrationDate = n.RegistrationDate,
                    Email = n.Email
                }).ToList()

            };

            return model;

        }

        /// <summary>
        /// Ajax-os autocomplete kereső metódus
        /// </summary>
        /// <param name="currentUserId">Jelenlegi bejelentkezett felhasználó azonosítója</param>
        /// <param name="selectedUser">Begépelt felhasználónév vagy egy része</param>
        /// <returns>Hasonlító entitások listája</returns>
        public List<UserViewModel> SearchForUser(string currentUserId, string selectedUser)
        {

            var model = UnitOfWork.GetDbSet<ApplicationUser>().Where(x => x.CanList
                && x.EmailConfirmed
                && x.Id != currentUserId
                && x.FullName.ToLower().Contains(selectedUser.ToLower()))
            .Select(n => new UserViewModel
            {
                UserId = n.Id,
                UserName = n.UserName,
                UserFullName = n.FullName,
                RegistrationDate = n.RegistrationDate,
                Email = n.Email
            }).ToList();

            return model;

        }
        /// <summary>
        /// Vissza adja azonokat a felhasználókat melyek a bejelentkezett felhasználónak látható és nem tiltott
        /// </summary>
        /// <param name="currentUserId">bejelentkezett felhasználó azonosítója</param>
        /// <param name="selectedUser">User vagy user név részlet</param>
        /// <returns>List<UserViewModel></returns>
        public List<UserViewModel> SearchForBanUser(string currentUserId, string selectedUser)
        {
            var bannedUserIds = UnitOfWork.GetDbSet<Ban>()
                .Where(x => x.WhoID == currentUserId)
                .Select(u => u.WhooseID);

            var model = UnitOfWork.GetDbSet<ApplicationUser>().Where(x => x.CanList
                && x.EmailConfirmed
                && x.Id != currentUserId
                && !bannedUserIds.Contains(x.Id)
                && x.FullName.ToLower().Contains(selectedUser.ToLower()))
            .Select(n => new UserViewModel
            {
                UserId = n.Id,
                UserName = n.UserName,
                UserFullName = n.FullName,
                RegistrationDate = n.RegistrationDate,
                Email = n.Email
            }).ToList();

            return model;

        }

        /// <summary>
        /// Törli egy felhasználó ban listájának egy tagját
        /// </summary>
        /// <param name="userId">Bejelentkezett felhasználó azonosító</param>
        /// <param name="whoose">Törölni kívánt felhasználó azonosító</param>
        public void RemoveBann(string userId, string whoose)
        {
            using (var t = UnitOfWork.BeginTransaction())
            {
                try
                {
                    var removedEntity = UnitOfWork.GetDbSet<Ban>().FirstOrDefault(x => x.WhoID == userId && x.WhooseID == whoose);

                    if (removedEntity == null)
                        throw new Exception();

                    UnitOfWork.GetDbSet<Ban>().Remove(removedEntity);

                    UnitOfWork.SaveChanges();
                    t.Commit();
                }
                catch (Exception)
                {
                    t.Rollback();
                    throw;
                }

            }

        }
        /// <summary>
        /// Bannol egy felhasználót
        /// </summary>
        /// <param name="userId">Bejelentkezett felhasználó azonosítója</param>
        /// <param name="whoose">Bannolni kívánt felhasználó azonosítója</param>
        public void AddBann(string userId, string whoose)
        {
            using (var t = UnitOfWork.BeginTransaction())
            {
                try
                {
                    //Önmagát nem bannolhatja
                    if (userId == whoose)
                        throw new Exception();

                    var insertEntity = new Ban
                    {
                        WhoID = userId,
                        WhooseID = whoose
                    };

                    //Ha már tartalmazza nem szúrjuk be
                    if (!UnitOfWork.GetDbSet<Ban>().Any(x => x.WhoID == userId && x.WhooseID == whoose))
                        UnitOfWork.GetDbSet<Ban>().Add(insertEntity);

                    UnitOfWork.SaveChanges();
                    t.Commit();
                }
                catch (Exception)
                {
                    t.Rollback();
                    throw;
                }

            }
        }
        /// <summary>
        /// Bannolt felhasználók listáját adja vissza.
        /// </summary>
        /// <param name="userId">Bejelentkezett felhasználó azonosító</param>
        /// <returns>List<UserViewModel></returns>
        public List<UserViewModel> GetBanList(string userId)
        {
            var model = UnitOfWork.GetDbSet<Ban>().Where(x => x.WhoID == userId).Select(b => new UserViewModel
            {
                UserId = b.Whoose.Id,
                UserName = b.Whoose.UserName,
                UserFullName = b.Whoose.FullName

            }).ToList();

            return model;
        }


        public bool GetVisibility(string currentUserId)
        {
            var userEntity = UnitOfWork.GetDbSet<ApplicationUser>().FirstOrDefault(u => u.Id == currentUserId);

            if (userEntity == null)
                throw new Exception();

            return userEntity.CanList;
        }

        public void SetVisibility(string currentUserId, bool visibility)
        {
            var userEntity = UnitOfWork.GetDbSet<ApplicationUser>().FirstOrDefault(u => u.Id == currentUserId);

            if (userEntity == null)
                throw new Exception();

            using (var t = UnitOfWork.BeginTransaction())
            {
                try
                {
                    userEntity.CanList = visibility;

                    UnitOfWork.SaveChanges();
                    t.Commit();

                }
                catch (Exception ex)
                {
                    t.Rollback();
                    throw;
                }
            }
        }
    }
}
